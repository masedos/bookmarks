##Bookmarks

## Blog: https://bookmarks-masedos.c9.io/

Tutorial by https://django-generic-bookmarks.readthedocs.org/en/latest/#

1) Initialized empty Git repository in ~/bookmarks/.git/

    $ git init  
    $ git config user.name "Fernandes Macedo"
    $ git config user.email masedos@egmail.com
    $ git status
    $ git add -A .
    $ git commit -m "first commit bookmarks"

2) Create a new repository on github tinny

    $ git remote add origin https://masedos@bitbucket.org/masedos/bookmarks.git
    $ git push -u origin master


## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

3) Run migrate command to sync models to database and create Django's default superuser and auth system

    $ python manage.py makemigrations
    $ python manage.py migrate

4) Run Django

    $ python manage.py runserver $IP:$PORT
    
5) Link
    sudo ln -nsf /usr/bin/python3.4  /usr/bin/python